<?php
require_once '../classes/ParserCSV.php';
require_once '../classes/Algorithm.php';
require_once '../classes/SessionWrapper.php';

SessionWrapper::start();

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(!empty($_POST['knapsack']) && (int) $_POST['knapsack'] > 0 && !empty($_POST['algorithm'])) {
        $knapsackSize = (int) $_POST['knapsack'];

        $chosenAlgorithm = $_POST['algorithm'];

        $parser = new ParserCSV();
        $algorithm = new Algorithm();
        $algorithm->setKnapsack($knapsackSize);
        $knapsackItems = [];

        $file = $parser->loadCSV('../instancja.csv');

        if(!$file) {
            $msg = "<p class=\"alert alert-danger\">Brak pliku!!!</p>";
            SessionWrapper::set('error', $msg);
        }

        $items = $parser->readCSV($file);

        switch($chosenAlgorithm){
            case 'greedyDecide':
                $knapsackItems = $algorithm->greedyDecideKnapsack($items);
                break;
        }

        // using session to send data to index.php
        SessionWrapper::set('size', $algorithm->getKnapsack());
        SessionWrapper::set('knapsack', $knapsackItems);
        header('Location: ../index.php');

    } else {
        $msg = "<p class=\"alert alert-danger\">Nieprawidłowa pojemność plecaka!!!</p>";
        SessionWrapper::set('error', $msg);
        header('Location: ../index.php');
    }
}