<?php

class Item
{
    private $itemId;
    private $itemWeight;
    private $itemValue;
    private $unitWeight;

    /**
     * Item constructor.
     * @param $itemId
     * @param $itemWeight
     * @param $itemValue
     * @internal param $unitWeight
     */
    public function __construct($itemId, $itemWeight, $itemValue)
    {
        $this->itemId = $itemId;
        $this->itemWeight = $itemWeight;
        $this->itemValue = $itemValue;
        $this->unitWeight = round($this->itemValue / $this->itemWeight, 2);

    }

    /**
     * @return mixed
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * @return mixed
     */
    public function getItemWeight()
    {
        return $this->itemWeight;
    }

    /**
     * @return mixed
     */
    public function getItemValue()
    {
        return $this->itemValue;
    }

    /**
     * @return float|int
     */
    public function getUnitWeight()
    {
        return $this->unitWeight;
    }


    /**
     * @param mixed $itemId
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
    }

    /**
     * @param mixed $itemWeight
     */
    public function setItemWeight($itemWeight)
    {
        $this->itemWeight = $itemWeight;
    }

    /**
     * @param mixed $itemValue
     */
    public function setItemValue($itemValue)
    {
        $this->itemValue = $itemValue;
    }


}