<?php

require_once 'Item.php';

class Algorithm
{
    private $knapsack;

    /**
     * Algorithm constructor.
     */
    public function __construct()
    {
        $this->knapsack = 0;
    }

    /**
     * @return int
     */
    public function getKnapsack()
    {
        return $this->knapsack;
    }

    /**
     * @param int $knapsack
     */
    public function setKnapsack($knapsack)
    {
        if ($knapsack > 0) {
            $this->knapsack = $knapsack;
        } else {
            $this->knapsack = 0;
        }

    }

    /**
     * method that is packing items to knapsack using greedy decide algorithm for knapsack problem
     * @param $items
     * @return array
     */
    public function greedyDecideKnapsack($items)
    {
        $packedItems = [];
        $i = 0;
        $knapsack = $this->getKnapsack();

        $items = $this->sortByUnitWeight($items);


        foreach ($items as $item) {
            if ($item->getItemWeight() <= $knapsack) {
                $packedItems[$i] = $item;
                $knapsack = $knapsack - $item->getItemWeight(); // decreasing knapsack size
                $i++;
            }
        }

        return $packedItems;
    }

    /**
     * method that sorts array of items by unitWeight using bubble sort algorithm from largest to smallest
     * @param $array
     * @return bool
     */
    private function sortByUnitWeight($array)
    {
        if (!$array) {
            return false;
        }

        for ($i = 1; $i < count($array); $i++) {
            for ($j = count($array) - 1; $j >= $i; $j--) {
                if ($array[$j - 1]->getUnitWeight() < $array[$j]->getUnitWeight()) {
                    list($array[$j - 1], $array[$j]) = [$array[$j], $array[$j - 1]];
                }
            }
        }

        return $array;
    }
}