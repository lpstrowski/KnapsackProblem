<?php

class SessionWrapper
{
    private static $sessionStarted = false;

    /**
     * @param $key
     * @param $value
     */
    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * @param $key
     * @return bool
     */
    public static function get($key)
    {
        if(isset($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return false;
        }
    }

    public static function start()
    {
        if(self::$sessionStarted == false) {
            session_start();
            self::$sessionStarted = true;
        }
    }

    public static function destroy()
    {
        if(self::$sessionStarted == true) {
            session_destroy();
        }
    }
}