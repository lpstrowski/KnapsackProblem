<?php

require_once 'Item.php';

class ParserCSV
{
    private $file;

    /**
     * @param $path
     * @return array|bool
     */
    public function loadCSV($path)
    {
        if(file_exists($path)) {
            $this->file = file($path);
        } else {
            return false;
        }

        return $this->file;
    }


    /**
     * @param $file
     * @return array
     */
    public function readCSV($file)
    {
        $items = [];
        $i = 0;

        foreach ($file as $line) {
            $line = explode(';', $line);

            if((int)trim($line[0]) != 0 && (int)trim($line[1]) != 0 && (int)trim($line[2]) != 0) {
                $items[$i] = new Item((int)$line[0], (int)$line[1], (int)$line[2]);
                $i++;
            }
        }

        return $items;
    }

    /**
     * @param $items
     * @return int
     */
    public function getValuesSum($items)
    {
        $sum = 0;
        foreach ($items as $item) {
            $sum += $item->getItemValue();
        }

        return $sum;
    }

    /**
     * @param $items
     * @return int
     */
    public function getTotalWeight($items)
    {
        $sum = 0;
        foreach ($items as $item) {
            $sum += $item->getItemWeight();
        }

        return $sum;
    }
}