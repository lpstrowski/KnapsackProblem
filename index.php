<?php
require_once 'classes/ParserCSV.php';
require_once 'classes/Item.php';
require_once 'classes/SessionWrapper.php';

SessionWrapper::start();

$parser = new ParserCSV();

$file = $parser->loadCSV('instancja.csv');

if(!$file) {
    SessionWrapper::set('error', 'Brak pliku do odczytu danych!!!');
}

$items = $parser->readCSV($file);
?>

<!DOCTYPE html>
<html lang="pl-PL">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
              integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <title>Main page</title>
    </head>
    <body>
    <header id="main-header">
        <h1>Problem plecakowy (KNAPSACK PROBLEM)</h1>
    </header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <h2>Lista wszystkich przedmiotów</h2>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Item ID</th>
                            <th>Item weight</th>
                            <th>Item value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($items as $item) :?>
                        <tr>
                            <td><?= $item->getItemId()?></td>
                            <td><?= $item->getItemWeight()?></td>
                            <td><?= $item->getItemValue()?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Suma</th>
                            <th><?= $parser->getTotalWeight($items) ?></th>
                            <th><?= $parser->getValuesSum($items) ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="col-md-4">
                <h2>Utwórz plecak do spakowania przedmiotów</h2>
                <form class="form-horizontal" action="modules/runAlgorithm.php" method="POST">
                    <label class="control-label" for="number">Podaj rozmiar plecaka :</label>
                    <input type="number" name="knapsack" id="number">
                    <label class="control-label" for="algorithm">Wybierz algorytm :</label>
                    <select name="algorithm" id="algorithm">
                        <option value="greedyDecide">Zachłanny decyzyjny</option>
                    </select>
                    <input type="submit" name="runAlgorithm" value="Uruchom algorytm">
                </form>
            </div>
            <div class="col-md-4">
                <div class="error">
                    <h4>
                       <?= SessionWrapper::get('error') ?>
                    </h4>
                </div>
                <div class="calculated">
                    <h2>Lista wszystkich przedmiotów w plecaku</h2>
                    <p>Rozmiar plecaka : <strong><?= SessionWrapper::get('size') ?></strong></p>
                    <?php $knapsackItems = SessionWrapper::get('knapsack'); ?>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Item ID</th>
                                <th>Item weight</th>
                                <th>Item value</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($knapsackItems as $knapsackItem) :?>
                            <tr>
                                <td><?= $knapsackItem->getItemId()?></td>
                                <td><?= $knapsackItem->getItemWeight()?></td>
                                <td><?= $knapsackItem->getItemValue()?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Suma</th>
                            <th><?= $parser->getTotalWeight($knapsackItems) ?></th>
                            <th><?= $parser->getValuesSum($knapsackItems) ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <footer id="footer">
        <div class="container-fluid">
            <p class="text-muted">Copyright 2016 &copy; Łukasz Pstrowski. All Rights Reserved.</p>
        </div>
    </footer>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"
            integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
    </body>
</html>
<?php

SessionWrapper::destroy();